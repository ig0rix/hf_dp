package headfirst.strategy.my;

/**
 * Created by igorshestakov on 16/12/13.
 */
public class Squeak implements QuackBehavior{
    @Override
    public void Quack() {
        System.out.println("Squeak!");
    }
}
