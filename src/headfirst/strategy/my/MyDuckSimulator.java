package headfirst.strategy.my;

/**
 * Created by igorshestakov on 16/12/13.
 */
public class MyDuckSimulator {

    public static void main (String[] args){
        Duck mullardDuck = new MullardDuck();
        mullardDuck.performQuack();
        mullardDuck.performFly();
        mullardDuck.setFlyBehavior(new FlyRocketPowered());
        mullardDuck.performFly();
    }
}
