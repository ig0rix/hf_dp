package headfirst.strategy.my;

/**
 * Created by igorshestakov on 17/12/13.
 */
public class FlyRocketPowered implements FlyBehavior {

    @Override
    public void Fly() {
        System.out.println("I'm fling with a rocket!");
    }

}
