package headfirst.strategy.my;

/**
 * Created by igorshestakov on 17/12/13.
 */
public class ModelDuck extends Duck {

    public ModelDuck() {
        flyBehavior = new FlyNoWay();
    }

    @Override
    void display() {
        System.out.println("I'm a model duck!");
    }
}
