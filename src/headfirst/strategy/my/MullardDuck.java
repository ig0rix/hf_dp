package headfirst.strategy.my;

/**
 * Created by igorshestakov on 16/12/13.
 */
public class MullardDuck extends Duck {
    public MullardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    @Override
    void display() {
            System.out.println("I'm mullarduck!");
    }
}
